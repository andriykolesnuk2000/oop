#include "worker.h"
#include <time.h>
#include <iostream>
#include <windows.h>

Worker::Worker()
{

}

Worker::Worker(QString name, int year, int month, Company workPlace)
{
    Name = name;
    Year = year;
    Month = month;
    WorkPlace = workPlace;
}

int Worker::GetWorkExperience()
{
    SYSTEMTIME st;
    GetSystemTime(&st);

    return (st.wMonth - Month) + (st.wYear - Year) * 12;
}

int Worker::GetTotalMoney()
{
    return GetWorkExperience() * WorkPlace.GetSalary();
}


QString Worker::GetName()
{
    return Name;
}

void Worker::SetName(QString name)
{
    Name = name;
}


int Worker::GetYear()
{
    return Year;
}

void Worker::SetYear(int year)
{
    Year = year;
}


int Worker::GetMonth()
{
    return Month;
}

void Worker::SetMonth(int month)
{
    Month = month;
}


Company Worker::GetWorkPlace()
{
    return WorkPlace;
}

void Worker::SetWorkPlace(Company workPlace)
{
    WorkPlace = workPlace;
}
