#include "company.h"

Company::Company()
{

}

Company::Company(QString name, QString position, int salary)
{
    Name = name;
    Position = position;
    Salary = salary;
}

QString Company::GetName()
{
    return Name;
}

void Company::SetName(QString name)
{
    Name = name;
}


QString Company::GetPosition()
{
    return Position;
}

void Company::SetPosition(QString position)
{
    Position = position;
}


int Company::GetSalary()
{
   return Salary;
}

void Company::SetSalary(int salary)
{
    Salary = salary;
}
