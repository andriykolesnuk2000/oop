#ifndef WORKER_H
#define WORKER_H
#include <QCoreApplication>
#include "company.h"

class Worker
{
public:
    Worker();
    Worker(QString name, int year, int month, Company workPlace);

    QString GetName();
    void SetName(QString name);

    int GetYear();
    void SetYear(int year);

    int GetMonth();
    void SetMonth(int month);

    Company GetWorkPlace();
    void SetWorkPlace(Company workPlace);

    int GetWorkExperience();
    int GetTotalMoney();

private:
    QString Name;
    int Year;
    int Month;
    Company WorkPlace;
};

#endif // WORKER_H
