#include <QCoreApplication>

#include <QTextCodec>
#include <iostream>
//#include <QString>
#include <QTextStream>
#include "worker.h"
#include <QDebug>
#include <Windows.h>
QTextStream cout(stdout);
QTextStream cin(stdin);

//using namespace std;



Worker* ReadWorkersArray(const int count)
{
    Worker *workers = new Worker[count];

    QString name;
    int year;
    int month;

    QString companyName;
    QString position;
    int salary;

    for(int i = 0; i < count; i++)
    {
        cout << "Enter year: ";
        cout.flush();
        cin >> year;
        system("color 02");

        cout << "Enter name: ";
        cout.flush();
        name = cin.readLine();
        name = cin.readLine();
        system("color 03");

        cout << "Enter month: ";
        cout.flush();
        cin >> month;
        system("color 04");

        cout << "Enter company name: ";
        cout.flush();
        companyName = cin.readLine();
        companyName = cin.readLine();
        system("color 05");

        cout << "Enter position: ";
        cout.flush();
        position = cin.readLine();
        system("color 06");

        cout << "Enter salary: ";
        cout.flush();
        cin >> salary;
        system("color 07");

        workers[i] = Worker(name, year, month, Company(companyName, position, salary));
    }
    system("color 07");
    return workers;
}

void PrintWorker(Worker worker)
{

     cout << QString::fromUtf8("Name: ").toLocal8Bit().data() << worker.GetName();
     cout << QString::fromUtf8("Year: ").toLocal8Bit().data() << worker.GetYear();
     cout << QString::fromUtf8("Month: ").toLocal8Bit().data() << worker.GetMonth();
     cout << QString::fromUtf8("Company name: ").toLocal8Bit().data() << worker.GetWorkPlace().GetName();
     cout << QString::fromUtf8("Position: ").toLocal8Bit().data() << worker.GetWorkPlace().GetPosition();
     cout << QString::fromUtf8("Salary: ").toLocal8Bit().data() << worker.GetWorkPlace().GetSalary() << endl;
     cout.flush();
}

void PrintWorkers(Worker* workers, int count)
{
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    for(int i = 0; i < count; i++)
    {

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 1));
         cout << QString::fromUtf8("Name: ").toLocal8Bit().data() << workers[i].GetName();
         cout.flush();

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 2));
         cout << QString::fromUtf8(" Year: ").toLocal8Bit().data() << workers[i].GetYear();
         cout.flush();

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 3));
         cout << QString::fromUtf8(" Month: ").toLocal8Bit().data() << workers[i].GetMonth();
         cout.flush();

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 4));
         cout << QString::fromUtf8(" Company name: ").toLocal8Bit().data() << workers[i].GetWorkPlace().GetName();
         cout.flush();

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 5));
         cout << QString::fromUtf8(" Position: ").toLocal8Bit().data() << workers[i].GetWorkPlace().GetPosition();
         cout.flush();
         //cout << QString::fromUtf8(" Work experiance: ").toLocal8Bit().data() << workers[i].GetWorkExperience();

         SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 6));
         cout << QString::fromUtf8(" Salary: ").toLocal8Bit().data() << workers[i].GetWorkPlace().GetSalary() << endl << endl;

         cout.flush();
    }
SetConsoleTextAttribute(hStdOut, (WORD)((0 << 4) | 7));
}

void GetWorkersInfo(Worker* workers, int count, int& maxSalary, int& minSalary)
{
    maxSalary = workers[0].GetWorkPlace().GetSalary();
    minSalary = workers[0].GetWorkPlace().GetSalary();

    int currentSalary;

    for(int i = 1; i < count; i++)
    {
        currentSalary = workers[i].GetWorkPlace().GetSalary();

        if(maxSalary < currentSalary)
                maxSalary = currentSalary;

        if(minSalary > currentSalary)
                minSalary = currentSalary;
    }
}

void SortWorkerBySalary(Worker* workers, int count)
{
    for(int i = 0; i < count - 1; i++)
    {
        for(int j = 0; j < count - (i + 1); j++)
        {
            if(workers[j].GetWorkPlace().GetSalary() < workers[j + 1].GetWorkPlace().GetSalary())
            {
                Worker temp = workers[j];
                workers[j] = workers[j + 1];
                workers[j + 1] = temp;
            }
        }
    }
}

void SortWorkerByWorkExperience(Worker* workers, int count)
{
    for(int i = 0; i < count - 1; i++)
    {
        for(int j = 0; j < count - (i + 1); j++)
        {
            if(workers[j].GetWorkExperience() > workers[j + 1].GetWorkExperience())
            {
                Worker temp = workers[j];
                workers[j] = workers[j + 1];
                workers[j + 1] = temp;
            }
        }
    }
}



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

#ifdef Q_OS_WIN32
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
#endif

#ifdef Q_OS_LINUX
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
#endif

    Worker* workers;
    workers = ReadWorkersArray(3);
    //PrintWorker(workers[0]);
    PrintWorkers(workers, 3);

    int max, min;

    GetWorkersInfo(workers, 3, max, min);

    cout << "Max salary: " << max << endl;
    cout << "Min salary: " << min << endl << endl << endl;;

    SortWorkerBySalary(workers, 3);

    cout << "Workers after sort by salary:" << endl;
    PrintWorkers(workers, 3);


    SortWorkerByWorkExperience(workers, 3);

    cout << "Workers after sort by Work Experience:" << endl;

    PrintWorkers(workers, 3);

    //delete workers;

    return a.exec();    
}
