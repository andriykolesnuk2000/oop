#ifndef COMPANY_H
#define COMPANY_H
#include <QCoreApplication>

class Company
{
public:
    Company();
    Company(QString name, QString position, int salary);

    QString GetName();
    void SetName(QString name);

    QString GetPosition();
    void SetPosition(QString position);

    int GetSalary();
    void SetSalary(int salary);

private:
    QString Name;
    QString Position;
    int Salary;
};

#endif // COMPANY_H
