﻿// lab4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
#include <list>
#include <random>
#include <time.h>

using namespace std;

void PrintVector(vector<double> vd)
{
	cout << "Vector: ";
	for (auto iter = vd.begin(); iter != vd.end(); iter++)
	{
		cout << *iter << "  ";
	}
	cout << endl;
}

void PrintList(list<double> ld)
{
	cout << "List: ";
	for (auto iter = ld.begin(); iter != ld.end(); iter++)
	{
		cout << *iter << "  ";
	}
	cout << endl;
}


void InitVecrot(vector<double>& vd)
{
	cout << "Vector" << endl;

	double currentEl;

	vd.push_back((rand() % 100) / 10.0);
	
	currentEl = (rand() % 100) / 10.0;

	if (currentEl > vd[0])
	{
		vd.push_back(currentEl);
	}
	else vd.insert(vd.begin(), currentEl);

	cout << vd.capacity() << endl;

	for (size_t i = 0; i < 20; i++)
	{
		currentEl = (rand() % 100) / 10.0;

		for (size_t j = 0; j < vd.size(); j++)
		{
			if (currentEl < vd[j]) 
			{
				vd.insert(vd.begin() + j, currentEl);
				break;
			}			
		}
		if (currentEl > vd[vd.size() - 1])
			vd.insert(vd.end(), currentEl);
		cout << vd.capacity() << endl;
	}
}

void InitList(list<double>& ld)
{
	cout << "List" << endl;

	double currentEl;

	ld.push_back((rand() % 100) / 10.0);

	for (size_t i = 0; i < 20; i++)
	{
		currentEl = (rand() % 100) / 10.0;

		for (auto iter = ld.begin(); iter != ld.end(); iter++)
		{
			if (currentEl > ld.back()) 
			{
				ld.push_back(currentEl);
				break;
			}				

			if (currentEl < *iter)
			{
				ld.insert(iter, currentEl);
				break;
			}
		}
	}
}


template <class T>
void GetMaxMinAverrage(vector<T> vd)
{
	cout << "Vector" << endl;

	T max = *vd.begin();
	T min = *vd.begin();
	T all = *vd.begin();	

	for (auto iter = ++vd.begin(); iter != vd.end(); iter++)
	{
		if (*iter < min)
			min = *iter;

		if (*iter > max)
			max = *iter;

		all += *iter;
	}
	cout << "Мiнiмальне значення:" << min << endl;
	cout << "Максимальне значення:" << max << endl;
	cout << "Середнє значення:" << all/vd.size() << endl;
}

template <class T>
void GetMaxMinAverrage(list<T> ld)
{
	cout << "List" << endl;

	T max = *ld.begin();
	T min = *ld.begin();
	T all = *ld.begin();

	for (auto iter = ++ld.begin(); iter != ld.end(); iter++)
	{
		if (*iter < min)
			min = *iter;

		if (*iter > max)
			max = *iter;

		all += *iter;
	}
	cout << "Мiнiмальне значення:" << min << endl;
	cout << "Максимальне значення:" << max << endl;
	cout << "Середнє значення:" << all / ld.size() << endl;

}


int CalcNumSites(vector<double> vd)
{
	cout << "Vector" << endl;

	int i = 0, j, count = 0;

	for (auto iter = ++vd.begin(); iter != vd.end(); iter++, i = j)
	{
		i++;
		
		j = i;
		while ((j < vd.size()) && (*iter >= *(iter - 1)))
		{
			++iter;
			++j;			
		}
		if ((j - i) >= 1)
			++count;

		i = j;
		while ((j < vd.size()) && (*iter <= *(iter - 1)))
		{
			++iter;
			++j;
			if (iter == vd.end()) {
				++count;
				return count;
			}
		}
		if ((j - i) >= 1)
			++count;
		
		if (iter == vd.end())
			break;
	}
	cout << "Кiлькiсть промiжкiв монотонностi:" << count << endl;
	return count;
}

int CalcNumSites(list<double> ld)
{
	cout << "List" << endl;

	int i = 0, j, count = 0;
	list<double>::iterator _iter;
	for (auto iter = ++ld.begin(); iter != ld.end(); iter++, i = j)
	{
		i++;
		_iter = iter;
		j = i;
		while ((j < ld.size()) && (*iter >= *(--_iter)))
		{
			++iter;
			_iter = iter;			
			++j;			
		}
		if ((j - i) >= 1)
			++count;

		i = j;
		while ((j < ld.size()) && (*iter <= *(--_iter)))
		{
			++iter;
			_iter = iter;
			++j;			
		}
		if ((j - i) >= 1)
			++count;

		if (iter == ld.end())
			break;
	}
	cout << "Кiлькiсть промiжкiв монотонностi:" << count << endl;
	return count;
}


vector<double> CreateNewVector(vector<double> vd) 
{
	vector<double> newVector;
	double el = 0;
	int i = 0;
	for (auto iter = vd.begin(); iter != vd.end(); iter++, i++)
	{
		for(auto newIter = vd.begin(); newIter != (iter + 1); newIter++)
		{
			el += *newIter;
		}
		newVector.push_back(el / (i + 1));
		el = 0;
	}
	return newVector;
}

list<double> CreateNewList(list<double> ld)
{
	list<double> newList;
	double el = 0;
	int i = 0;
	list<double>::iterator _iter;
	for (auto iter = ld.begin(); iter != ld.end(); iter++, i++)
	{
		_iter = iter;
		for (auto newIter = ld.begin(); newIter != (++_iter); newIter++)
		{
			_iter = iter;
			el += *newIter;
		}
		newList.push_back(el / (i + 1));
		el = 0;
	}
	return newList;
}

int main()
{
	setlocale(LC_ALL, "Ukrainian");

	vector<double> vd;
	InitVecrot(vd);
	cout << endl << endl << endl;

	list<double> ld;
	InitList(ld);
	cout << endl << endl << endl;

	PrintVector(vd);
	cout << endl << endl << endl;
	PrintList(ld);
	cout << endl << endl << endl;

	GetMaxMinAverrage<double>(vd);
	cout << endl << endl << endl;
	GetMaxMinAverrage<double>(ld);
	cout << endl << endl << endl;

	//CalcNumSites(vd);

	
	
	
	auto f = [](vector<double> vd) -> int
	{
		cout << "Vector" << endl;

		int i = 0, j, count = 0;

		for (auto iter = ++vd.begin(); iter != vd.end(); iter++, i = j)
		{
			i++;

			j = i;
			while ((j < vd.size()) && (*iter >= *(iter - 1)))
			{
				++iter;
				++j;
			}
			if ((j - i) >= 1)
				++count;

			i = j;
			while ((j < vd.size()) && (*iter <= *(iter - 1)))
			{
				++iter;
				++j;
				if (iter == vd.end()) {
					++count;
					return count;
				}
			}
			if ((j - i) >= 1)
				++count;

			if (iter == vd.end())
				break;
		}
		cout << "Кiлькiсть промiжкiв монотонностi:" << count << endl;
		return count;
	};

	f(vd);

	cout << endl << endl << endl;

	auto f1 = [](list<double> ld) 
	{
		cout << "List" << endl;

		int i = 0, j, count = 0;
		list<double>::iterator _iter;
		for (auto iter = ++ld.begin(); iter != ld.end(); iter++, i = j)
		{
			i++;
			_iter = iter;
			j = i;
			while ((j < ld.size()) && (*iter >= *(--_iter)))
			{
				++iter;
				_iter = iter;
				++j;
			}
			if ((j - i) >= 1)
				++count;

			i = j;
			while ((j < ld.size()) && (*iter <= *(--_iter)))
			{
				++iter;
				_iter = iter;
				++j;
			}
			if ((j - i) >= 1)
				++count;

			if (iter == ld.end())
				break;
		}
		cout << "Кiлькiсть промiжкiв монотонностi:" << count << endl;
		return count;
	};

	
	f1(ld);
	cout << endl << endl << endl;

	vector<double> newVector = CreateNewVector(vd);
	PrintVector(newVector);
	cout << endl << endl << endl;
	list<double> newList = CreateNewList(ld);
	PrintList(newList);

	getchar();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
