﻿// lab3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>


using namespace std;
int Size = 10;


void Print(int* mas, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << mas[i] << "  ";
	}
}

void printAddreses(int* mas, int n)
{
	cout <<"Адрес першого елемента масивву  " << &mas[0] << endl;
	cout <<"Адрес останнього елемента масивву  " << &mas[n-1] << endl;
}

void Reverse(int* mas, int n)
{
	for (int i = 0; i < n / 2; i++)
	{
		swap(mas[i], mas[n - (1 + i)]);
	}
}

int Delete(int* mas, int forDelete)
{
	int countDeleted = 0;
	for (int i = 0; i < Size; i++)
	{
		if (mas[i] == forDelete)
		{
			for (int j = i; j < Size - 1; j++)
			{
				mas[j] = mas[j + 1];
			}
			i--;
			Size--;
			countDeleted++;
		}
	}
	return countDeleted;
}

void InsertZero(int* mas)
{
	for (int i = Size - 1; i >= 0; i--)
	{
		if (mas[i] % 2 == 0)
		{
			for (int j = Size - 1; j >= i; j--)
			{
				mas[j + 1] = mas[j];
			}
			mas[i] = 0;
			Size++;			
		}
	}
}

void PrindMatrAdreses(int **mas, int n, int m)
{
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if ((i + j) % 2 == 0)
				cout << &mas[i][j] << " ";
			else
				cout << "\t";
		}
		cout << endl;
	}

}

int main()
{
	setlocale(LC_ALL, "ru");

	int* mas = new int[20];

	for (int i = 0; i < Size; i++)
	{
		mas[i] = rand() % 50;
	}

	cout << "Масив:" << endl;
	Print(mas, Size);

	cout << endl << endl;

	printAddreses(mas, Size);
	Reverse(mas, Size);

	cout << endl << endl;

	cout << "Перевернутий масив:" << endl;
	Print(mas, Size);

	cout << endl << endl;

	cout << "Видаляю з масива елементи зi значенням 0: видалено " << Delete(mas, 0) << " елементiв" << endl;
	cout << "Масив пiсля видалення:" << endl;
	Print(mas, Size);

	cout << endl << endl;

	cout << "Масив пiсля вставки 0:" << endl;	
	InsertZero(mas);
	Print(mas, Size);

	cout << endl << endl << endl << endl;

	int **dmas = new int*[4];
	for (int i = 0; i < 4; i++)
		dmas[i] = new int[4];

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			dmas[i][j] = rand() % 10;
		}
	}
	
	cout << "Вивожу адреси елементiв матрицi з пaрною сумою iндексiв:" << endl;
	PrindMatrAdreses(dmas, 4, 4);

	delete mas;

	getchar();

	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
