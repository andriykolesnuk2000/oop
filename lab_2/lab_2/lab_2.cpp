﻿// lab_2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <random>

using namespace std;

const int N = 10;

double CalculateIf(int num)
{
	if (num == 44)
	{
		return 14.15 * 10;
	}
	else if (num == 41)
	{
		return 11.98 * 10;
	}
	else if (num == 57)
	{
		return 12.69 * 10;
	}
	else if (num == 37)
	{
		return 15 * 10;
	}
	else
		return 0;
}

double CalculateSwitch(int num)
{
	switch(num)
	{
	case 44: 
		return 14.15 * 10;
	case 41:
		return 11.98 * 10;
	case 57:
		return 12.69 * 10;
	case 37:
		return 15 * 10;
	default:
		return 0;
	}
}

//static int NumSites(int arr[], int N)
//{
//	bool inc;	
//	int NumSitesOfMon = 0;
//	//int i = 1;
//	
//	cout << "NumSites" << endl;
//
//	for (int i = 0; i < N; i++)
//	{
//		cout << arr[i] << "  ";
//	}
//	cout << endl << endl;
//
//	if (arr[0] < arr[1])
//	{
//		inc = true;
//		NumSitesOfMon++;		
//	}
//	else
//	{
//		inc = false;
//		NumSitesOfMon++;		
//	}
//	int i = 1;
//	for(i = 1; i < 9; i++)
//	{		
//		//i = 1;
//		if (arr[i] < arr[i + 1])
//		{			
//			if(inc == false)
//			{
//				inc = true;
//				NumSitesOfMon++;
//			}			
//		}
//
//		if(arr[i] > arr[i + 1])
//		{			
//			if (inc == true)
//			{
//				inc = false;
//				NumSitesOfMon++;
//			}
//		}
//	}
//
//	return NumSitesOfMon;
//}

int CalcNumSites(int arr[], int n)
{
	int i, j, count = 0;
	for (i = 1; i < n; i = j) 
	{
		j = i;
		while ((j < n) && (arr[j] >= arr[j - 1]))
			++j;
		if ((j - i) >= 1)
			++count;

		i = j;
		while ((j < n) && (arr[j] <= arr[j - 1]))
			++j;
		if ((j - i) >= 1)
			++count;
	}
	return count;
}

double* CreateMas(int arr[])
{	
	static double B[N];
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			B[i] += arr[j];
		}
		B[i] = B[i] / (i + 1);
	}
	return B;
}

void SortMas(int arr[5][5], int n, int m)
{
	
	for (int j = 0; j < m; j = j + 2)
	{
		for (int i = 0; i < n; i++)
		{
			for (int k = 0; k < n - (1 + i); k++)
			{
				if (arr[k][j] < arr[k + 1][j])
				{
					int tmp = arr[k][j];
					arr[k][j] = arr[k + 1][j];
					arr[k + 1][j] = tmp;
				}
			}
		}
	}
	
}

int main()
{
	setlocale(LC_ALL, "ru");

	cout << CalculateIf(37) << endl;
	cout << CalculateSwitch(37) << endl;
		

	int arr[N] = {1, 5, 6, 2, 1, 7, 9, 0, 5, 1};
	

	cout << "Масив А: ";
	for (int i = 0; i < 10; i++)
	{
		cout << arr[i] << "  ";
	}
	cout << endl << endl;

	cout << "Промiжкiв монотонностi: " << CalcNumSites(arr, N) << endl << endl;

	double* B = CreateMas(arr);

	cout << "Масив B: ";
	for (int i = 0; i < 10; i++)
	{
		cout << B[i] << "  ";
	}

	int squareArr[5][5];

	for (size_t i = 0; i < 5; i++)
	{
		for (size_t j = 0; j < 5; j++)
		{
			squareArr[i][j] = rand() % 20;
		}
	}

	cout << endl << endl << endl;
	for (size_t i = 0; i < 5; i++)
	{
		for (size_t j = 0; j < 5; j++)
		{
			cout << squareArr[i][j] << "\t";
		}
		cout << endl;
	}
	cout << endl << endl << endl;

	SortMas(squareArr, 5, 5);


	for (size_t i = 0; i < 5; i++)
	{
		for (size_t j = 0; j < 5; j++)
		{
			cout << squareArr[i][j] << "\t";
		}
		cout << endl;
	}
	cout << endl << endl << endl;	

	getchar();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
