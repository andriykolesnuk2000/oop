﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <math.h>

using namespace std;

#define PI            3.14159265358979323846

double ToDegrees(double angle)
{
	return angle * (180 / PI);
}

double ToRadian(double angle)
{
	return angle / 180 * PI;
}

void Swap(double& a, double& b)
{
	a = a + b;
	b = a - b;
	a = a - b;
}

void Triangle1(double a, double b, double gama)
{
	gama = ToRadian(gama);
	double S = 0.5 * a * b * sin(gama);
	double c = sqrt(a*a + b * b - 2 * b*a*cos(gama));
	double P = a + b + c;
	double alfa = asin(a / (c / sin(gama)));
	double beta = asin(b / (c / sin(gama)));
	double h = (2 * S) / c;

	cout << "Сторона А = " << a << endl;
	cout << "Сторона В = " << b << endl;
	cout << "Сторона C = " << c << endl << endl;

	cout << "Кут A = " << ToDegrees(alfa) << endl;
	cout << "Кут B = " << ToDegrees(beta) << endl;
	cout << "Кут C = " << ToDegrees(gama) << endl << endl;

	cout << "Висота до сторони С = " << h << endl;
	cout << "Периметр = " << P << endl;
	cout << "Площа = " << S << endl << endl << endl;
	//cout << ToDegrees(beta);
}

void Triangle2(double h, double c, double alfa)
{
	alfa = ToRadian(alfa);
	double S = 0.5 * c * h;
	double b = S / (0.5 * c * sin(alfa));	
	double a = sqrt(b*b + c * c - 2 * b*c*cos(alfa));
	double beta = asin(b / (a / sin(alfa)));
	double gama = asin(c / (a / sin(alfa)));
	double P = a + b + c;

	cout << "Сторона А = " << a << endl;
	cout << "Сторона В = " << b << endl;
	cout << "Сторона C = " << c << endl << endl;

	cout << "Кут A = " << ToDegrees(alfa) << endl;
	cout << "Кут B = " << ToDegrees(beta) << endl;
	cout << "Кут C = " << ToDegrees(gama) << endl << endl;

	cout << "Висота до сторони С = " << h << endl;
	cout << "Периметр = " << P << endl;
	cout << "Площа = " << S << endl << endl << endl;

}

void Triangle3(double a, double b, double h)
{
	double gama = acos(h/a) + acos(h/b);
	Triangle1(a, b, ToDegrees(gama));
	/*double S = 0.5 * a * b * sin(gama);
	double c = sqrt(a*a + b * b - 2 * b*a*cos(gama));
	double P = a + b + c;
	double alfa = asin(a / (c / sin(gama)));
	double beta = asin(b / (c / sin(gama)));
	double h = (2 * S) / c;

	cout << "Сторона А = " << a << endl;
	cout << "Сторона В = " << b << endl;
	cout << "Сторона C = " << c << endl << endl;

	cout << "Кут A = " << ToDegrees(alfa) << endl;
	cout << "Кут B = " << ToDegrees(beta) << endl;
	cout << "Кут C = " << ToDegrees(gama) << endl << endl;

	cout << "Висота до сторони С = " << h << endl;
	cout << "Периметр = " << P << endl;
	cout << "Площа = " << S << endl;
	//cout << ToDegrees(beta);*/
}

int main()
{
	setlocale(LC_ALL, "ru");

	
	double x, y, z;

	x = 2.444;
	y = 0.869E-2;
	z = -130.0;

	double s = ((pow(x, y + 1)*exp(y - 1)) / (1 + x * abs(y - tan(z))))*(1 + abs(y-x)) + pow(abs(y - x), 2) / 2 - pow(abs(y - x), 3) / 3;

	std::cout << "s = " << s << endl << endl << endl;

	double A = 1, B = 2, C = 3;

	std::cout << "A = " << A << endl << "B = " << B << endl << "C = " << C << endl << endl;

	Swap(A, C);
	std::cout << "Пiсля обмiну A i C" << endl;
	std::cout << "A = " << A << endl << "B = " << B << endl << "C = " << C << endl << endl;

	Swap(C, B);
	std::cout << "Пiсля обмiну C i B" << endl;
	std::cout << "A = " << A << endl << "B = " << B << endl << "C = " << C << endl << endl;

	Swap(B, A);

	std::cout << "Пiсля обмiну B i A" << endl;
	std::cout << "A = " << A << endl << "B = " << B << endl << "C = " << C << endl << endl;

	double angle = 1.5 * PI;
	
	

	std::cout << "кут в радiанах = " << angle / PI << " Пi" << endl;
	std::cout << "кут в градусах = " << ToDegrees(angle) << endl;

	//cout.precision(3);

	//std::cout << round(sin(PI));

	Triangle1(2, 3, 60);

	Triangle2(1.9639, 2.6457, 40.8934);

	Triangle3(2, 3, 1.9639);

	getchar();
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
