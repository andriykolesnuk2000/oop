#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "vector.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_plussButton_clicked();

    void on_minusButton_clicked();

    void on_multiplyButton_clicked();

    void on_changeButton_clicked();

    void PrintResult(MyVector mv);

    void InitializeVectors();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
