#include "widget.h"
#include "ui_widget.h"




Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}
int MyVector::count = 0;
MyVector mv1;
MyVector mv2;

void Widget::on_plussButton_clicked()
{
    InitializeVectors();

    //auto mv3 = mv1 +mv2;

    PrintResult(mv1 + mv2);
}

void Widget::on_minusButton_clicked()
{
    InitializeVectors();

    PrintResult(mv1 - mv2);
}

void Widget::on_multiplyButton_clicked()
{
    mv1.Set_i(ui->LEVector1I->text().toDouble());
    mv1.Set_j(ui->LEVector1J->text().toDouble());

    PrintResult(mv1 * ui->LEVector2I->text().toDouble());
}

void Widget::on_changeButton_clicked()
{
    InitializeVectors();

    mv1 = -mv1;
    mv2 = -mv2;

    QString vector1("(" + QString::number(mv1.Get_i()) + ", " + QString::number(mv1.Get_j()) + ")");
    QString vector2("  (" + QString::number(mv2.Get_i()) + ", " + QString::number(mv2.Get_j()) + ")");

    ui->resultLabel->setText("Результат: " + vector1 + vector2);
}

void Widget::PrintResult(MyVector mv)
{
    ui->resultLabel->setText("Результат: (" + QString::number(mv.Get_i()) + ", " + QString::number(mv.Get_j()) + ")");
    ui->countLabel->setText("Кількість створених векторів: " + QString::number(MyVector::GetCount()));
}

void Widget::InitializeVectors()
{
    mv1.Set_i(ui->LEVector1I->text().toDouble());
    mv1.Set_j(ui->LEVector1J->text().toDouble());

    mv2.Set_i(ui->LEVector2I->text().toDouble());
    mv2.Set_j(ui->LEVector2J->text().toDouble());
}


















