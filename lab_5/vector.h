#ifndef VECTOR_H
#define VECTOR_H
#include <math.h>

class MyVector
{
private:
    double i;
    double j;
    static int count;
public:

    static void IncCount()
    {
        count++;
    }

    static int GetCount()
    {
        return count;
    }

    MyVector()
    {
        MyVector::IncCount();
    }
    MyVector(double _i, double _j)//: i(_i), j(_j)
    {
        i = _i;
        j = _j;
        MyVector::IncCount();
    }


    MyVector(double lenght, int angle)
    {
        double deg = angle / 180 * 3.14;
        i = cos(deg) * lenght;
        j = sin(deg) * lenght;
        MyVector::IncCount();
    }


    MyVector(const MyVector& mv) //: i(mv.i), j(mv.j)
    {
        i = mv.i;
        j = mv.j;
        MyVector::IncCount();
    }

    MyVector operator+ (const MyVector& mv) const
    {
        MyVector mv1;
        mv1.Set_i((double)(i + mv.i));
        mv1.Set_j((double)(j + mv.j));

        return mv1;
    }

    MyVector operator- (const MyVector& mv) const
    {
        return MyVector((double)(i - mv.i), (double)(j - mv.j));
    }

    MyVector operator- () const
    {
        return MyVector((double)(- i), (double)(- j));
    }

    MyVector operator* (const int coef) const
    {
        return MyVector(i*coef, j*coef);
    }

    double Get_i()
    {
        return i;
    }
    double Get_j()
    {
        return j;
    }

    void Set_i(double i)
    {
        this->i = i;
    }

    void Set_j(double j)
    {
        this->j = j;
    }
};


#endif // VECTOR_H
